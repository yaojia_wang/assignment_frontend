import React, { PureComponent, Fragment } from 'react';
import { Table, Alert } from 'antd';
import styles from './index.less';

class ReservationTable extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props;
  }

  handleTableChange = (pagination, filters, sorter) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(pagination, filters, sorter);
    }
  };

  render() {
    const { data = {}, rowKey, ...rest } = this.props;
    const list = data.data;
   

    return (
      <div className={styles.reservationTable}>        
        <Table
          rowKey={rowKey || 'key'}
          dataSource={list}
          onChange={this.handleTableChange}
          {...rest}
        />
      </div>
    );
  }
}

export default ReservationTable;

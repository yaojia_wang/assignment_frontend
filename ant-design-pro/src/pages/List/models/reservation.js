import { queryReservations, addReservation } from '@/services/api';

export default {
  namespace: 'reservation',

  state: {
    data: [],
    pagination: {},
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryReservations, payload);      

      yield put({
        type: 'save',
        payload: response,
      });
    },
    *add({ payload, callback }, { call, put }) {
      const response = yield call(addReservation, payload);

      if(response.success){
        yield put({
          type: 'save',
          payload: response,
        });
      }
      
      if (callback){
         callback(response.isSuccess);
      }
    },    
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};

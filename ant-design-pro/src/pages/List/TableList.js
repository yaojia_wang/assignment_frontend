import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Icon,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  TimePicker,
  Upload
} from 'antd';
import ReservationTable from '@/components/ReservationTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './TableList.less';

const TimeFormat = 'HH:mm';
const FormItem = Form.Item;
const { TextArea } = Input;

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const { getFieldDecorator } = form;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="Add Reservation"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 7 }} wrapperCol={{ span: 15 }} label="Email">
        {form.getFieldDecorator('email', {
          rules: [
                  { required: true, 
                    message: 'Email Is Required',
                  },
                  {  
                    message: 'Please provide a correct email address.',
                    pattern: new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
                  }],
        })(<Input placeholder="Reservation Email" />)}
      </FormItem>
      <FormItem labelCol={{ span: 7 }} wrapperCol={{ span: 15 }} label="Name">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: 'Name Is Required' }],
        })(<Input placeholder="Name" />)}
      </FormItem>
      <FormItem labelCol={{ span: 7 }} wrapperCol={{ span: 15 }} label="Phone Number">
        {form.getFieldDecorator('phoneNumber', {
          rules: [{ required: true, message: 'Phone Is Required' }],
        })(<Input placeholder="Phone" />)}
      </FormItem>
      <FormItem labelCol={{ span: 7 }} wrapperCol={{ span: 15 }} label="Start Time">
        {form.getFieldDecorator('startTime', {
            initialValue: moment(),
            rules: [{
              required: true, 
              message: 'Start Time Is Required' 
            }, 
            (rule, value, callback) => {
              var endTime = form.getFieldValue('endTime');
              if (value && endTime && endTime.isBefore(value)) {
                  callback('Start Time Must Before End Time')
              }
              callback()
            }],
          })(<TimePicker 
                format={TimeFormat} />)}
      </FormItem>
      <FormItem labelCol={{ span: 7 }} wrapperCol={{ span: 15 }} label="End Time">
      {form.getFieldDecorator('endTime', {
          initialValue: moment(),
          rules: [{ 
                  required: true, 
                  message: 'End Time Is Required' 
                  }, 
                  (rule, value, callback) => {
                    var startTime = form.getFieldValue('startTime');
                    if (value && startTime && value.isBefore(startTime)) {
                        callback('End Time Must After Start Time')
                    }
                    callback()
                  }
                ],
        })(<TimePicker 
              format={TimeFormat} />)}
      </FormItem>
    </Modal>
  );
});


/* eslint react/no-multi-comp:0 */
@connect(({ reservation, loading }) => ({
  reservation,
  loading: loading.models.reservation,
}))

@Form.create()
class TableList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    formValues: {},
    stepFormValues: {},
  };

  columns = [
    {
      title: 'Reservation Id',
      dataIndex: 'id',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      sorter: true, 
    },
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
    },     
    {
      title: 'Start Time',
      dataIndex: 'startTime',
    },
    {
      title: 'End Time',
      dataIndex: 'endTime',
    },  
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'reservation/fetch',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'reservation/fetch',
      payload: params,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };
  
  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'reservation/add',
      payload: {
        email: fields.email,
        name: fields.name,
        phoneNumber: fields.phoneNumber,
        startTime: fields.startTime.format(TimeFormat),
        endTime: fields.endTime.format(TimeFormat),
      },
      callback: (isSuccess) =>
      {
        if(isSuccess){
          message.success('Add Reservation Successfully.');
          dispatch({
            type: 'reservation/fetch'
          });
        }
        else{
          message.error('Add Reservation Fail.');
        }
      }
    })
    
    this.handleModalVisible();
  };

  onUploadChange = (info) => {
    const { dispatch } = this.props;
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} with message: ${info.file.response.message}`);
          dispatch({
            type: 'reservation/fetch',
          });
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
  };

  render() {
    const uploadProps = {
      name: 'file',
      action: '/api/Reservations/upload',     
      beforeUpload(file) {
        const isJson = file.type === 'application/json';
        if (!isJson) {
          message.error('You can only upload Json file!');
        }
        return isJson
      },

      onChange: this.onUploadChange        
    };

    const {
      reservation: { data },
      loading,
      form
    } = this.props;
    const { modalVisible } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <PageHeaderWrapper title="Reservations">
        <Card bordered={false}>
          <div className={styles.tableList}>        
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                Create
              </Button>
              <Upload {...uploadProps}>
                <Button>
                  <Icon type="upload" /> Upload Reservations
                </Button>
              </Upload>              
            </div>            
            <ReservationTable
              loading={loading}
              data={data}
              rowKey="id"
              columns={this.columns}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />        
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryReservations() {
  return request('/api/reservations');
}

export async function addReservation(params) {
  return request('/api/reservations', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}


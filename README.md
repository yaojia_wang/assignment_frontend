**Thank you for your time to check this assignment**

This project is base on React, with a UI libry Ant Design (https://ant.design/docs/react/introduce).
Ant Design provide a scaffold also, so this project structure is base on that scaffold.

To init the project:
$ npm install

To start the project:
$ npm run start:no-mock

The scaffold support mock data, with this comment we without using any mock data.

The next step is to make some config:

In config/config.js:
```
 proxy: {
    '/api/reservations': {
      target: 'https://localhost:44321',
      changeOrigin: true,      
      secure: false
    },
  },
```
  
 Change to corresponse URL in target.
 If still using https, then just keep `secure: false`, otherwise you can remove this line.
 
 
